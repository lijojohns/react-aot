import React, { useState } from 'react';
import { connect } from 'react-redux';
import { Form, Button }from 'react-bootstrap';
import { submitForm } from './homeActions';
import './home.scss';


function Home(props) {

    const { submitForm } = props;
    const [formData, setFormData] = useState({
        query: "",
        message: ""
    });

    const updateFormData = event =>
    setFormData({
      ...formData,
      [event.target.name]: event.target.value
    });

    const { query, message } = formData;

    const processData = () => {
        debugger
        submitForm(formData);
    }



    return (
        <div className="container mt-4">
            <Form>
                <Form.Group controlId="formBasicQuery">
                    <Form.Label>Query</Form.Label>
                    <Form.Control type="text" placeholder="" value={query} name="query"
                    onChange={e => updateFormData(e)} />
                </Form.Group>

                <Form.Group controlId="formBasicText" className="mt-4">
                    <Form.Label>Message</Form.Label>
                    <Form.Control type="text" className="textarea" placeholder="" value={message} name="message"
                    onChange={e => updateFormData(e)}  />
                </Form.Group>
                <Button className="btnSubmit mt-4" onClick={()=> processData()} variant="primary" type="button">
                    Submit
                </Button>
            </Form>
        </div>
    );
}


const mapStateToProps = (state) => {
    return { }
  }
  
  const mapDispatchToProps = (dispatch) => {
    return { 
      submitForm: (formData) => dispatch(submitForm(formData))
    }
  }

export default connect(mapStateToProps, mapDispatchToProps)(Home)