const initialState = {
    processedData: {}
}

const homeReducer = (state = initialState, action) => {
    switch (action.type) {
        case 'PROCESS_DATA': return {
            ...state,
            processedData: action.payload
        }
        default: return state
    }
}

export default homeReducer;