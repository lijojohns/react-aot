import { combineReducers } from 'redux';
import homeReducer from '../components/home/homeReducer';
const rootReducer = combineReducers({homeReducer: homeReducer})

export default rootReducer