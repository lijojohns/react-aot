
import { createStore, applyMiddleware } from 'redux'
import thunk from 'redux-thunk'
// import cakeReducer from '../components/cake/cakeReducer'
import rootReducer from './rootReducer'

const store = createStore(rootReducer, applyMiddleware(thunk))

export default store