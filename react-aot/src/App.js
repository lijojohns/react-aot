import './App.css';
import { Navbar, NavDropdown, Nav } from 'react-bootstrap';
import { Provider } from 'react-redux';
import store from './redux/store';
import Home from './components/home/home';


function App() {
  return (
    <Provider store={store}>
      <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
        <Navbar.Brand href="#" className="brand">AOT Technologies</Navbar.Brand>
        <Navbar.Collapse id="responsive-navbar-nav" className="navend">
          <Nav>
            <Nav.Link href="#deets">Profile</Nav.Link>
            <Nav.Link eventKey={2} href="#memes">
              Logout
            </Nav.Link>
          </Nav>
        </Navbar.Collapse>
      </Navbar>

      <Home />
    </Provider>
  );
}

export default App;
